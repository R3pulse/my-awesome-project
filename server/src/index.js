const express = require('express');
const bodyParser = require('body-parser');
const fetch = require('node-fetch');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const mongoose = require('mongoose');
const myGraphQLSchema = require('./graphql/Schema');
const ObjectId = require('mongodb').ObjectID;


const app = express();

// Mongo configs
const db =   mongoose.connect('mongodb://mongo/ratings', {
  useMongoClient: true,
})
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  // we're connected!
  console.log('Connected to Mongo');
});

// Enable mongo promises
mongoose.Promise = global.Promise;

//require all models
// fs.readdirSync(__dirname + '/../models').forEach(function(filename)  {
//   if(~filename.indexOf('.js')) require( __dirname + './models/' + filename)
// })

const Rating = mongoose.model('Rating', { rating: Number, uuid: String })

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// bodyParser is needed just for POST.
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema: myGraphQLSchema }));
app.get('/graphiql', graphiqlExpress({ endpointURL: '/graphql' })); // if you want GraphiQL enabled

// Fetch locations from fake service
const getLocations = async () => {
  const koskiData = await fetch('http://fake-services:3000/fake-services/fishing/all-locations');
  return koskiData.json();
};

// respond with "hello world" when a GET request is made to the homepage
app.get('/', async (req, res) => {
  res.json({ ok: 'ok' });
});

app.get('/locations', async (req, res) => {
  const locations = await getLocations();
  res.json(locations);
});

app.use('/rating', bodyParser.json())
app.post('/rating', (req, res) => {
  let rate = new Rating({ rating: req.body.rating, uuid: req.body.id })
  rate.save((err) => {
    if (err) {
      return res.status(500).json({ message: err })
    }
    res.status(200).json({ message: 'post created!' })
  })
})

app.get('/rating/:uuid', (req, res) => {
  Rating.aggregate(
    { $match : { uuid : { $regex: req.params.uuid } } },
    { $group:
      {
        _id: "$uuid",
        average: { $avg: "$rating" }
      }
    },
    { $project: { _id: 0, average: 1 } }
  ).exec((err, locations) => {
    if(err) {
      res.status(500).send(err)
    }
    if(!locations) {
      return null
    }
    return res.status(200).send(locations)
  })
})


app.listen(3000, () => console.log('Node server is listening on port 3000!'));
