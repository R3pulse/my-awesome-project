import React from 'react'
import PropTypes from 'prop-types';
import { Card, Icon, Button, Row, Col } from 'react-materialize'
import ReactStars from 'react-stars'

const LocationView = ({ location, cleanState, ratingChanged, locationRating, locationImage }) => (
  <div>
    <Row>
      <Button id="back-button" onClick={cleanState}><Icon>close</Icon></Button>
    </Row>
      <Row>
        <Col s={12} className="location-name"> <h5>{ location.name }</h5> </Col>
      </Row>
      <Row className="location-prices-row">
        <Card>
          { location.prices.map(price => (
              <Row key={price.price} className="location-info">
                  <Row>
                    <Col s={2}>{price.price}</Col>
                  </Row>
                  <Row>
                    <Col s={2}>{price.type}</Col>
                  </Row>
              </Row>
            ))
          }
          {locationImage &&
            <Col s={4} className="location-image">
              <img src={locationImage} />
            </Col>
          }
        </Card>
      </Row>
        <ReactStars
          count={5}
          value={locationRating}
          onChange={ratingChanged}
          size={24}
          color2={'#ffd700'}
          className="stars"
        />
  </div>
)

LocationView.proptypes = {
    location: PropTypes.object.isRequired,
    cleanState: PropTypes.func.isRequired,
    ratingChanged: PropTypes.func.isRequired,
    locationImage: PropTypes.string,
}

export default LocationView
