import React from 'react'
import PropTypes from 'prop-types';
import { Button, Row, Col } from 'react-materialize'

const LocationRow = ({ location, showLocationView }) => (
  <div>
      <Row className="location-row">
        <Col s={2} />
        <Col s={6}> <h5>{ location.name } </h5> </Col>
        <Button s={2} onClick={() => {showLocationView(location)}}>More info</Button>
      </Row>
  </div>
)

Location.proptypes = {
    location: PropTypes.object.isRequired,
    showLocationView: PropTypes.func.isRequired,
}

export default LocationRow
