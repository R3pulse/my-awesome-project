import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';
import styled from 'styled-components';
import './App.css';
import { Footer, Button, Icon, Row, Col, Input } from 'react-materialize'
import LocationRow from './Components/LocationRow'
import LocationView from './Components/LocationView'

// My awesome styled component
const Wrapper = styled.div`

`;

class App extends Component {
  constructor(props) {
    super(props);
    this.showLocationView = this.showLocationView.bind(this);
    this.state = {
      input: '',
      locations: [],
      location: this.props.location,
      locationView: false,
      locationRating: 0,
      locationImage: '',
    };
  }

  static propTypes = {
    data: PropTypes.shape({
      locations: PropTypes.arrayOf(
        PropTypes.shape({
          title: PropTypes.string,
          name: PropTypes.string,
          price: PropTypes.string,
        }),
      ),
    }),
  };


  componentDidMount = async () => {
    this.fetchAllLocations()
  }

  fetchAllLocations = async () => {
    const allLocations = await fetch('http://localhost:4000/locations')
    .then( res => res.json())
    this.setState({
      locations: allLocations,
    })
  }

  fetchRatings = async (id) => {
    const rating = await fetch(`http://localhost:4000/rating/${id}`).then(res => res.json())
    if(!rating[0]) {
      return 0
    }
    return rating[0].average
  }

  clickHandler = () => {
    const { input } = this.state;
    const { mutate } = this.props;
    mutate({
      variables: { locationTitle: input },
    });
  };

  inputChange = e => {
    this.setState({
      input: e.target.value,
    });
  };

  showLocationView = (l) => {
    this.fetchRatings(l.id).then((r) => {
      const locationImage = l.gdata.error_message ? null : `http://maps.googleapis.com/maps/api/staticmap?center=${l.gdata.results[0].formatted_address}&zoom=10&size=300x200&markers=${l.gdata.results[0].formatted_address}&sensor=false`
      this.setState({
        locationView: true,
        location: l,
        locationRating: Math.round(r),
        locationImage: locationImage,
      })
    })
  }

  cleanState = () => {
    this.setState({
      locationView: false,
      location: {},
      locationImage: '',
    })
  }

  ratingChanged = (newRating) => {
      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      fetch('http://localhost:4000/rating', {
        headers: myHeaders,
        method: 'POST',
        body: JSON.stringify({ rating: newRating, id: this.state.location.id })
      })
      .then((res) => {
        if (res.status >= 200 && res.status < 300) {
          return res
        } else {
          let error = new Error(res.statusText)
          error.res = res
          throw error
        }
      })
  }

  render() {
    const { data: { loading } } = this.props;
    if (loading) return null;

    return (
      <div>
      {!this.state.locationView &&
        <Wrapper>
          <Row>
            <Col className="header-text">
                <h2> Kalamiähen Komrad </h2>
            </Col>
          </Row>
          <div>
            <Row className="search-row">
              <Col s={4}/>
              <Col s={2}>
                <Input type="text" label="Location" value={this.state.input} onChange={this.inputChange} className="list-search"><Icon>search</Icon></Input>
              </Col>
              <Col s={2} className="search-button"><Button onClick={this.clickHandler}>Kuikka</Button></Col>
            </Row>
          </div>
          { this.state.locations.map(l => (
              <LocationRow
                key={l.id}
                location={l}
                showLocationView={this.showLocationView}
              />
            ))
          }
        </Wrapper>
      }
      {this.state.locationView &&
        <Row>
          <LocationView
            location={this.state.location}
            cleanState={this.cleanState}
            ratingChanged={this.ratingChanged}
            locationRating={this.state.locationRating}
            locationImage={this.state.locationImage}
          />
        </Row>
      }
      <Footer
        copyrights="© 2017 Copyright"
        className="app-footer"
        links={
          <ul>
            <li><a className="grey-text text-lighten-3" href="https://gitlab.com">Gitlab<i className="fa fa-gitlab footer-icons gitlab-icon" aria-hidden="true" styles="color: orange;"></i></a></li>
            <li><a className="grey-text text-lighten-3" href="https://facebook.com">Facebook<i className="fa fa-facebook footer-icons facebook-icon" aria-hidden="true" styles="color: orange;"></i></a></li>
            <li><a className="grey-text text-lighten-3" href="https://instagram.com">Instagram<i className="fa fa-instagram footer-icons instagram-icon" aria-hidden="true" styles="color: orange;"></i></a></li>
            <li><a className="grey-text text-lighten-3" href="https://twitter.com">Twitter<i className="fa fa-twitter footer-icons twitter-icon" aria-hidden="true" styles="color: orange;"></i></a></li>
          </ul>
        }
      >
          <h5 className="white-text">Footer Content</h5>
          <p className="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
      </Footer>
      </div>
    );
  }
}

// GraphQL

// Query from server
const query = gql`
  query TodoAppQuery {
    location {
      title
    }
  }
`;

// Call mutation
const mutation = gql`
  mutation fakeNews($locationTitle: String!) {
    fakeNews(title: $locationTitle) {
      title
    }
  }
`;

// Update local cache
const update = {
  options: {
    update: (proxy, { data: { fakeNews } }) => {
      // Get current data
      const data = proxy.readQuery({ query });

      // write changes
      proxy.writeQuery({
        query,
        data: {
          ...data,
          location: fakeNews,
        },
      });
    },
  },
};

export default compose(graphql(query), graphql(mutation, update))(App);
